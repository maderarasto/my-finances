<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Inertia\Inertia;

class AuthController extends Controller
{
    public function loginForm() {
        return Inertia::render('Login');
    }

    public function login() {

    }

    public function registerForm() {
        return Inertia::render('Register');
    }

    public function register() {

    }

    public function logout() {

    }
}
